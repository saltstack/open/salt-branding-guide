================
Salt style guide
================

Hi! The Salt style guide has moved to the Salt User Guide repository under the
``docs`` > ``topics`` folder:
`https://gitlab.com/saltstack/open/docs/salt-user-guide/-/tree/master/docs/topics
<https://gitlab.com/saltstack/open/docs/salt-user-guide/-/tree/master/docs/topics>`_
